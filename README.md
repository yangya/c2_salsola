This repository contains scripts for baited homology inference of photosynthesis genes, topology analysis, and Ks analyses for Delphine Tefarikis's Salsola data set.

Citation: Tefarikis et al. In prep

Each sequence is formated as taxonID@sequenceID, separated by the special character "@".

Note: File formats are hard coded and code may not give the most informative error message if your data has formatting issues.

Input file names look like: taxonID.pep.fa for peptide files in fasta format, and taxonID.cds.fa for CDS files in fasta format. These were generated using scripts from https://bitbucket.org/yanglab/phylogenomic_dataset_construction/

##Ks analysis
Code for the Ks analyses were modified from code in https://bitbucket.org/blackrim/clustering/

###Within-species Ks plots
Copy all the .pep.fa and .cds.fa files into the working directory. Also copy bin/ and synonymous_calc.py into the working dir. cd into the working dir, and run:

	python ks_plots_blastp.py <num_cores>
	
It will output an R script that you can run in R to plot the output.

###Between-species Ks plots 
Copy the .pep.fa and .cds.fa files into the working directory. Also copy bin/ and synonymous_calc.py into the working dir. cd into the working dir, and run:

	ks_between_taxa_blastn.py workingDIR taxonID1 taxonID2 num_cores

Similarly, it will output an R script that you can run in R to plot the output.	


##C4 genes
Code for the baited homolog tree analysis were modified from https://bitbucket.org/yangya/genome_walking_2016 to accommodate much smaller dataset with closely related species, slightly different taxonID formatting, and pre-filtered transcripts by Corset in updated phylogenomic inference pipeline.

Currently all input cds and pep files were pre-filtered by Corset after Trinity, before translation.

Separate baits downloaded from TAIR website by locus ID:
	
	python write_AT_baits.py 1_AT_baits_from_Delphine/ATbaits.pep.fa 2_AT_baits_separated_by_locus
	
This generated 50 fasta files, with names looks like locusID.pep.fa. I then generated the file bait.sh to run the baited homolog search and cleaning for each of the 50 loci. This file has 50 lines, and the first line looks like: 

	python bait_homologs.py 2_AT_baits_separated_by_locus/AT1G08650.pep.fa data_17taxa/ 10 2 
	
In which the dir "data_17taxa" contains the peptide files for each of the 17 taxa we used for this analysis. "10" means we take the top 10 SWIPE hits from each taxon. Running this script generates initial alignment and tree, refiled alignment and tree after cutting internal branches longer than 1, and a second round of cutting removing internal branches longer than 0.5. I manually checked each alignment and tree to decide which alignment would be the final alignment, removed odd seqs before start codon, and removed sequences that are clearly misassembled, and named the alignment .final.aln. Occasionally I had to run fasta_to_tree.py if the final fasta output is the best.

After finishing all 50, which took me several hours, I copied all the *.final.aln file to a new dir 3_homolog_mafft_alignment, unalign, and run PRANK to get more accurate alignments:

	python unalign_fasta.py 3_homolog_mafft_alignment
	python prank_wrapper.py 3_homolog_mafft_alignment 3_homolog_mafft_alignment .fa aa
	
I checked the alignment - they looked good, but mostly very conserved. Phylogenetic trees made from these aa alignments would suffer from lack of information. Need to back translate into CDS before generating more accurate trees. Remove arabidopsis baits before back translation to avoid very long branches due to saturation of the 3rd codon position:

	python unalign_fasta_noAT.py 3_homolog_mafft_alignment

Then move the new fasta files to 5_remove_Arabidopsis. Run prank wrapper:

	python prank_wrapper.py 5_remove_Arabidopsis 6_prank_alignment_17taxa .fa aa
	
Concatenate all the cds files to the file 17taxa.cds.fa:

	cat data_17taxa/*.cds.fa >17taxa.cds.fa
	
Back translation using pal2nal:

	python pal2nal_wrapper.py 6_prank_aa_alignment_17taxa 17taxa.cds.fa 7_pal2nal
	
Phyutility to remove columns with less than 30% occupancy (you can use phyx to do so if you have phyx installed):

	python phyutility_wrapper.py 7_pal2nal 0.3 aa
	
Final phylogenetic trees using raxml with 200 bootstrap replicate for each gene:

	python raxml_bs_wrapper.py 7_pal2nal 2 dna
	
Move the final trees to 8a_cds_trees. I was planning on also do the peptide trees and put them in 8b_pep_trees, but they don't seem that informative. 
