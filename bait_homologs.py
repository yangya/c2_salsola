"""
Input:

- prepare a input fasta file that contains sequences used as bait:
	genename.pep.fa

- prepare a directory with candidate sequence pep fasta files named
	taxonid.pep.fa
  for each sequence, the sequence ids look like taxonid@seqid

swipe, taking the top output, construct the homolog
and carry out two rounds of refinement
"""

import phylo3,newick3,os,sys, seq
import fasta_to_tree
import ntpath
import trim_tips
import cut_long_internal_branches
import tree_utils

def get_filename_from_path(path):
	head, tail = ntpath.split(path)
	return tail or ntpath.basename(head)
	
def get_file_path(path):
	if path[-1] == "/": path = path[:-1]
	head, tail = ntpath.split(path)
	if head == "": head = "."
	return head+"/",tail

def swipe_dir(query_fasta,pepdir,outdir,num_cores,max_num_hits=10,min_bitscore=50.0):
	"""
	swipe query_fasta against each data set in DIR with peptide fasta files that
	either end with .pep.fa, swipe on each one
	write output in outdir
	take the top num_to_bait hits ranked by bitscore
	evalue set to 10
	ignore all hits with bitscore less than 30% of the highest hit from the query
	"""
	if pepdir[-1] != "/": pepdir += "/"
	if outdir[-1] != "/": outdir += "/"
	num_cores = str(num_cores)
	max_num_hits = int(max_num_hits)
	gene_name = get_filename_from_path(query_fasta).split(".")[0]
	
	# swipe with each taxon
	pepfiles = [i for i in os.listdir(pepdir) if i.endswith(".pep.fa")]
	datasets = [i.split(".")[0] for i in pepfiles]
	print len(pepfiles),"input peptide files read"
	assert len(set(datasets)) == len(datasets),\
		"dataset name repeats. remove duplicated sets"
	temp_files = [] # will remove these later
	for i in os.listdir(pepdir):
		if i.endswith(".pep.fa"):
			if not os.path.exists(pepdir+i+".psd"):
				os.system("makeblastdb -in "+pepdir+i+" -parse_seqids -dbtype prot -out "+pepdir+i)
			swipe_outname = pepdir+i+"."+get_filename_from_path(query_fasta).split(".")[0]+".swipe"
			temp_files.append(swipe_outname)
			temp_files.append(swipe_outname+".hits")
			if not os.path.exists(swipe_outname):
				cmd = "swipe -d "+pepdir+i+" -i "+query_fasta+" -a "+num_cores
				cmd += " -p blastp -o "+swipe_outname+" -m 8 -e 10"
				print cmd
				os.system(cmd)
			assert os.path.exists(swipe_outname), \
				"swipe did not finish correctly"
			"""
			swipe output colums are:
			Query id, Subject id, % identity, alignment length, mismatches, 
			gap openings, q. start, q. end, s. start, s. end, e-value, bit score
			"""
			# summarize the hit seq ids
			if not os.path.exists(swipe_outname+".hits"):
				hit_tuples = [] # a list of tuples (hit, bitscore)
				with open(swipe_outname,"r") as infile:
					for line in infile:
						if len(line) < 3: continue # skip empty lines
						spls = line.strip().split("\t")
						query,hit,bitscore = spls[0],spls[1].replace("lcl|",""),float(spls[-1])
						if query != hit and bitscore >= min_bitscore:
							hit_tuples.append((hit,bitscore))
				
				out = [] # unique hit ids
				highest = 0.0 # store the highest bitscore
				for hit, bitscore in sorted(hit_tuples,key=lambda x:x[1],reverse=True):
					if highest == 0.0: highest = bitscore # record the first hit
					if bitscore < 0.3*highest: break # stop recording
					if hit not in out:
						out.append(hit)
					if len(out) == max_num_hits: break
				if len(out) == 0: print "Warning: No hits found"
				with open(swipe_outname+".hits","w") as outfile:
					for hit in out:
						print hit
						outfile.write(hit+"\n")
	
	# write output summary files .rawswipe, .filetered_hits, .swipe.fa
	print "Writing output fasta"
	outfile1 = open(outdir+gene_name+".rawswipe","w")
	outfile2 = open(outdir+gene_name+".filetered_hits","w")
	outfile3 = open(outdir+gene_name+".swipe.fa","w")
	query_seqids = [] # avoid seq id repeats
	with open(query_fasta,"r") as infile:
		for line in infile:
			outfile3.write(line) # copy over query seqs
			if line[0] == ">":
				query_seqids.append(line.strip()[1:])
	for i in os.listdir(pepdir):
		if i.endswith(".pep.fa") or i.endswith(".cdhit"):
			# write the gene_name.rawswipe file to outdir
			with open(pepdir+i+"."+get_filename_from_path(query_fasta).split(".")[0]+".swipe","r") as infile:
				for line in infile:
					outfile1.write(line)
			seqDICT = {} # a dict for each taxon, key is seq name, value is seq
			for s in seq.read_fasta_file(pepdir+i):
				seqDICT[s.name] = s.seq
			with open(pepdir+i+"."+get_filename_from_path(query_fasta).split(".")[0]+".swipe.hits","r") as infile:
				for line in infile:
					line = line.strip()
					if len(line) == 0: continue
					outfile2.write(line+"\n")
					if line not in query_seqids:
						outfile3.write(">"+line+"\n"+seqDICT[line]+"\n")
	outfile1.close()
	outfile2.close()
	outfile3.close()
	
	# remove intermediate files
	for i in temp_files: os.remove(i)

def refine_homolog(query_fasta,start_fasta,deep_paralog_cutoff,num_cores):
	gene_name = get_filename_from_path(query_fasta)[1].split(".")[0]
	outdir,fasta = get_file_path(start_fasta)
	#print outdir,fasta
	deep_paralog_cutoff = float(deep_paralog_cutoff)
	query_ids = [s.name for s in seq.read_fasta_file(query_fasta)]
	new_fasta = [] # list of output refined fasta files
	print outdir,fasta
	
	# make a tree from the start_fasta
	treefile = fasta_to_tree.fasta_to_tree(outdir,fasta,num_cores,"aa")
	if treefile == None: return []
	with open(treefile,"r") as infile:
		root = newick3.parse(infile.readline())
	#root = trim_tips.trim(root,relative_cutoff=deep_paralog_cutoff,absolute_cutoff=deep_paralog_cutoff*2)
	if os.path.exists(outdir+fasta+".pasta.aln-cln"):
		clnfile = outdir+fasta+".pasta.aln-cln"
	else: clnfile = outdir+fasta+".mafft.aln-cln"

	if root != None:
		#with open(tree+".tt.mm","w") as outfile:
		#	outfile.write(newick3.tostring(root)+"\n")
		subtrees = cut_long_internal_branches.cut_long_internal_branches(root,cutoff=deep_paralog_cutoff)
		count = 0
		base_name = fasta.split(".")[0]
		seqDICT = {} # key is seqid, value is seq
		for s in seq.read_fasta_file(start_fasta):
			seqDICT[s.name] = s.seq
		for tree in subtrees:
			if tree == None: continue
			label_set = set(tree_utils.get_front_labels(tree))
			if len(label_set) > 4 and len(label_set & set(query_ids)) > 0:
				count += 1
				with open(outdir+base_name+"_"+str(count)+".subtree","w") as outfile:
					outfile.write(newick3.tostring(tree)+";\n")
				with open(outdir+base_name+"_"+str(count)+".fa","w") as outfile:
					for seqid in tree_utils.get_front_labels(tree):
						try:
							outfile.write(">"+seqid+"\n"+seqDICT[seqid]+"\n")
						except:
							print seqid,"not found in fasta file"
				new_fasta.append(outdir+base_name+"_"+str(count)+".fa")
	return new_fasta
				
if __name__ == "__main__":
	if len(sys.argv) != 6:
		print "python bait_homologs.py query_pep_fa pepDIR num_to_bait num_cores outdir"
		sys.exit(0)
	
	query_fasta,pepdir,num_to_bait,num_cores,out = sys.argv[1:]
	# the query fasta name is gene_name.pep.fa
	gene_name = get_filename_from_path(query_fasta).split(".")[0]
						
	"""
	get the initial fasta files
	wipe query_fasta against each data set in DIR
	write output in outdir
	take the top num_to_bait hits ranked by bitscore
	evalue set to 10
	"""
	outdir = out+"/"+gene_name+"/"
	try: os.stat(outdir)
	except: os.mkdir(outdir)
	if not os.path.exists(outdir+gene_name+".swipe.fa"):
		swipe_dir(query_fasta=query_fasta,\
			pepdir=pepdir,\
			outdir=outdir,\
			num_cores=num_cores,\
			max_num_hits=num_to_bait)

	# first round of refine
	refined_fastas = refine_homolog(query_fasta=query_fasta,\
		start_fasta=outdir+gene_name+".swipe.fa",\
		deep_paralog_cutoff=1.0,
		num_cores=num_cores)
	print refined_fastas
	
	# second round of refine
	new_fastas = []
	if len(refined_fastas) > 0:
		for i in refined_fastas:
			new_fastas += refine_homolog(query_fasta=query_fasta,\
				start_fasta=i,\
				deep_paralog_cutoff=0.5,
				num_cores=num_cores)
	print new_fastas
	refined_fastas = new_fastas
				
	# if last round was using fastree, refine one more time
	if len(refined_fastas) > 0 and os.path.exists(outdir+gene_name+"_1.fasttree.tre"):
		for i in refined_fastas:
			refine_homolog(query_fasta=query_fasta,\
				start_fasta=i,\
				deep_paralog_cutoff=0.5,
				num_cores=num_cores)
			
