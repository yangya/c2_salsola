"""
Input:

- prepare a input fasta file that contains sequences used as bait:
	genename.pep.fa

- prepare a directory with candidate sequence pep fasta files named
	taxonid.pep.fa, or
	taxonid.pep.fa.cdhitent
  for each sequence, the sequence ids look like taxonid@seqid

swipe, taking the top 100 output (swipe only outputs top 100 hits)
bitscore > 50 and >= 20% bitscore of self-hits
construct the homolog tree
and carry out two rounds of refinement
replace the seqids to long id for visualization in figtree
"""

import phylo3,newick3,os,sys
from swipe_dir import swipe
import seq
import fasta_to_tree
import ntpath
import refine_homolog
import taxon_name_subst

# do not mask tips for these genomes, 43 in total
GENOMES = ["Achn","Acoerulea","Athaliana","Atrichopoda","Beta",\
		   "Brapa","Bstricta","Caan","Cclementina","Cila",\
		   "Coca","Cpapaya","Crubella","Csativus","Csinensis",\
		   "Dica","Egrandis","Elgu","Esalsugineum","Fvesca",\
		   "Gmax","Graimondii","Lusitatissimum","Mesculenta","Mguttatus",\
		   "Mtruncatula","Musa","Nenu","Osativa","Pheq",\
		   "Phoe","Ppersica","Ptrichocarpa","Rara","Rcommunis",\
		   "Slycopersicum","Spolyrhiza","Spurpurea","Tcacao","Utri.plus",\
		   "Vvinifera","Zmays","Spol"]

def get_filename_from_path(path):
	head, tail = ntpath.split(path)
	return tail or ntpath.basename(head)

if __name__ == "__main__":
	if len(sys.argv) != 5:
		print "python bait_homologs.py query_pep_fa_dir database num_cores outdir"
		sys.exit(0)
	
	query_fasta_dir,database,num_cores,out_loc = sys.argv[1:]
	for query_fasta in sorted(os.listdir(query_fasta_dir)):
		print query_fasta
		gene_name = query_fasta.split(".")[0]
		outdir = out_loc+"/"+gene_name+"/"
		try: os.stat(outdir)
		except: os.mkdir(outdir)
		swipe_outname = outdir+gene_name+".swipe"
		if not os.path.exists(swipe_outname):
			# swipe against all 43 genomes
			cmd = "swipe -d "+database+" -i "+query_fasta_dir+"/"+query_fasta+" -a "+num_cores
			cmd += " -p blastp -o "+swipe_outname+" -m 8 -e 10"
			print cmd
			os.system(cmd)
			assert os.path.exists(swipe_outname), "swipe did not finish correctly"
			"""
			swipe output colums are:
			Query id, Subject id, % identity, alignment length, mismatches, 
			gap openings, q. start, q. end, s. start, s. end, e-value, bit score
			"""		
		# write initial fasta file
		if not os.path.exists(swipe_outname+".fa"):
			outfile = open(swipe_outname+".fa","w")
			out_seqids = [] # avoid seq id repeats
			with open(query_fasta_dir+"/"+query_fasta,"r") as infile:
				for line in infile:
					outfile.write(line) # copy over query seqs
					if line[0] == ">":
						out_seqids.append(line.strip()[1:])
			seqDICT = {} # a dict for each taxon, key is seq name, value is seq
			for s in seq.read_fasta_file(database):
				seqDICT[s.name] = s.seq
			hit_tuples = [] # a list of tuples (hit, bitscore)
			with open(swipe_outname,"r") as infile:
				for line in infile:
					if len(line) < 3: continue # skip empty lines
					spls = line.strip().split("\t")
					query,hit,bitscore = spls[0],spls[1].replace("lcl|",""),float(spls[-1])
					if bitscore > 50.0:
						hit_tuples.append((hit,bitscore))	
			highest = 0.0 # store the highest bitscore
			for hit, bitscore in sorted(hit_tuples,key=lambda x:x[1],reverse=True):
				if highest == 0.0: highest = bitscore # record the fist and self hit
				if bitscore < 0.2*highest: break # stop recording
				if hit not in out_seqids:
					outfile.write(">"+hit+"\n"+seqDICT[hit]+"\n")
					out_seqids.append(hit)
			outfile.close()
		
		if len(seq.read_fasta_file(swipe_outname+".fa")) >= 4:
			refined_fastas = refine_homolog.refine(query_fasta=query_fasta_dir+"/"+query_fasta,\
				start_fasta=swipe_outname+".fa",\
				deep_paralog_cutoff=1.0,
				num_cores=num_cores)
		else: print "less than 4 seqs in",swipe_outname+".fa"



