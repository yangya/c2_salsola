"""
Check data set names for:
- Duplicated names
- Special characters other than digits, letters and _
- all names look like taxonID@seqID, and file names are the taxonID
- all peptide and cds names have are matched one-to-one
"""

import os,sys
from re import match

def filename_to_taxonid(filename):
	return(filename.split(".")[0])

def name_format_correct(DIR):
	"""
	Check these and return false if any problem exists
	- Duplicated names
	- Special characters other than digits, letters and _
	- all names look like taxonID@seqID, and file names are the taxonID
	"""
	for i in os.listdir(DIR):
		taxonID = i.split(".")[0]
		print i,taxonID
		infile = open(DIR+i,"rU")
		seqIDs = []
		for line in infile:
			if line[0] != ">": continue #only look at seq names
			taxon,seqID = (line.strip()[1:]).split("@")
			assert taxon == taxonID,\
				taxon+" taxon id do not match file name "+taxonID
			assert match("^[a-zA-Z0-9_]+$",seqID),\
				seqID+" seq id contains special characters other than letter, digit and '_'"
			seqIDs.append(seqID)
		repeats = {j:seqIDs.count(j) for j in seqIDs}
		print len(seqIDs),"sequences checked"
		all_names_uniq = True
		for j in repeats:
			rep = repeats[j]
			if rep > 1:
				print j,"repeats",rep,"times"
				all_names_uniq = False
		infile.close()
	return all_names_uniq


def pep_cds_match(pepDIR,cdsDIR):
	"""
	Check that all pep and cds files match
	Check all pep and cds seq ids match
	"""
	pepfiles = [i for i in os.listdir(pepDIR)]
	cdsfiles = [i for i in os.listdir(cdsDIR)]
	pep_taxonids = {filename_to_taxonid(i) for i in pepfiles}
	cds_taxonids = {filename_to_taxonid(i) for i in cdsfiles}
	cds_file_match = {filename_to_taxonid(i):i for i in cdsfiles} # key is taxon id, value is cds file name
	assert len(pepfiles) == len(cdsfiles) and pep_taxonids == cds_taxonids,\
		"pep and cds file names do not match"
	for pepfile in pepfiles:
		cdsfile = cds_file_match[filename_to_taxonid(pepfile)]
		print pepfile,cdsfile
		pepids = []
		with open(pepDIR+pepfile,"r") as infile:
			for line in infile:
				if line[0] == ">":
					pepids.append((line.strip())[1:])
		cdsids = []
		with open(cdsDIR+cdsfile,"r") as infile:
			for line in infile:
				if line[0] == ">":
					cdsids.append((line.strip())[1:])
		if set(pepids) != set(cdsids):
			print "seqs that are in ",pepfile,"but missing in",cdsfile, set(pepids)-set(cdsids)
			print "seqs that are in ",cdsfile,"but missing in",pepfile, set(cdsids)-set(pepids)


if __name__ =="__main__":
	if len(sys.argv) != 3:
		print "usage: python check_pep_cds_match.py pepDIR cdsDIR"
		sys.exit()
	
	pepDIR = sys.argv[1]+"/"
	cdsDIR = sys.argv[2]+"/"
	name_format_correct(pepDIR)
	name_format_correct(cdsDIR)
	pep_cds_match(pepDIR,cdsDIR)
