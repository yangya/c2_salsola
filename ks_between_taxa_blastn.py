"""
make sure that synonymous_calc.py and bin from
the kaks-calculator https://code.google.com/p/kaks-calculator/
are in the current directory.

create a directory with four files only:
taxonID1.pep.fa
taxonID1.cds.fa
taxonID2.pep.fa
taxonID2.cds.fa
"""

import sys,os
from Bio import SeqIO

if __name__ == "__main__":
	if len(sys.argv) != 5:
		print "python ks_between_taxa_blastn.py workingDIR taxonID1 taxonID2 num_cores"
		sys.exit(0)
	
	workingdir,taxon1,taxon2,num_cores = sys.argv[1:]
	if workingdir[-1] != "/": workingdir += "/"

	pep1 = workingdir+taxon1+".pep.fa"
	pep2 = workingdir+taxon2+".pep.fa"
	cds1 = workingdir+taxon1+".cds.fa"
	cds2 = workingdir+taxon2+".cds.fa"
	out  = workingdir+taxon1+"_"+taxon2
	
	# blastn and take top hit from both direction
	if not os.path.exists(taxon1+"_"+taxon2+".rawblastn"):
		cmd = "makeblastdb -in "+cds2+" -parse_seqids -dbtype nucl -out "+cds2
		print cmd
		os.system(cmd)
		cmd = "blastn -db "+cds2+" -query "+cds1+" -evalue 10 -num_threads "+str(num_cores)
		cmd += " -max_target_seqs 1 -out "+taxon1+"_"+taxon2+".rawblastn "
		cmd += "-outfmt '6 qseqid qlen sseqid slen frames pident nident length mismatch gapopen qstart qend sstart send evalue bitscore'"
		print cmd
		os.system(cmd)
		os.system("rm "+cds2+".n*")
		
	if not os.path.exists(taxon2+"_"+taxon1+".rawblastn"):
		cmd = "makeblastdb -in "+cds1+" -parse_seqids -dbtype nucl -out "+cds1
		print cmd
		os.system(cmd)
		cmd = "blastn -db "+cds1+" -query "+cds2+" -evalue 10 -num_threads "+str(num_cores)
		cmd += " -max_target_seqs 1 -out "+taxon2+"_"+taxon1+".rawblastn "
		cmd += "-outfmt '6 qseqid qlen sseqid slen frames pident nident length mismatch gapopen qstart qend sstart send evalue bitscore'"
		print cmd
		os.system(cmd)
		os.system("rm "+cds2+".n*")

	#filter for reciprocol best hits with sufficient pident and nident
	if not os.path.exists(taxon1+"_"+taxon2+"_RBH"):
		taxon1_hits = set() #tubles of hits that pass filter
		infile = open(taxon1+"_"+taxon2+".rawblastn","r")
		line_count = 0
		tuple_count = 0
		for line in infile:
			spls = line.split("\t")
			if len(spls) < 3: continue
			line_count += 1
			query,hit,pident,nident = spls[0],spls[2],float(spls[5]),int(spls[6])
			if pident < 50.0 or nident < 150: continue
			tuple_count += 1
			taxon1_hits.add((query,hit))
		infile.close()
		print taxon1,line_count,"lines read,",tuple_count,"hits added"
		
		taxon2_hits = set() #tubles of hits that pass filter
		infile = open(taxon2+"_"+taxon1+".rawblastn","r")
		line_count = 0
		tuple_count = 0
		for line in infile:
			spls = line.split("\t")
			if len(spls) < 3: continue
			line_count += 1
			query,hit,pident,nident = spls[0],spls[2],float(spls[5]),int(spls[6])
			if pident < 50.0 or nident < 150: continue
			tuple_count += 1
			taxon2_hits.add((hit,query)) #reverse order to match pairs
		infile.close()
		print taxon2,line_count,"lines read,",tuple_count,"hits added"
		
		rbh = taxon1_hits & taxon2_hits
		with open(out+"_RBH","w") as outfile:
			for hit in rbh:
				outfile.write(hit[0]+"\t"+hit[1]+"\n")

	# write out cds pairs and pep pairs
	if not os.path.exists(taxon1+"_"+taxon2+".cds.pairs.fa"):
		cdsDICT = {} #key is seqid, value is seq
		with open(cds1,"r") as handle:
			for record in SeqIO.parse(handle,"fasta"):
				cdsDICT[str(record.id)] = str(record.seq)
		with open(cds2,"r") as handle:
			for record in SeqIO.parse(handle,"fasta"):
				cdsDICT[str(record.id)] = str(record.seq)
				
		pepDICT = {} #key is seqid, value is seq
		with open(pep1,"r") as handle:
			for record in SeqIO.parse(handle,"fasta"):
				pepDICT[str(record.id)] = str(record.seq)
		with open(pep2,"r") as handle:
			for record in SeqIO.parse(handle,"fasta"):
				pepDICT[str(record.id)] = str(record.seq)
				
		infile = open(out+"_RBH","r")
		outfile1 = open(out+".pep.pairs.fa","w")
		outfile2 = open(out+".cds.pairs.fa","w")
		for line in infile:
			seqid1,seqid2 = line.strip().split("\t")
			#shorten taxon name so that the next step would run
			seqid1_short = seqid1.split("_")[0]+"@"+seqid1.split("@")[1]
			seqid2_short = seqid2.split("_")[0]+"@"+seqid2.split("@")[1]
			outfile1.write(">"+seqid1_short+"\n"+pepDICT[seqid1]+"\n")
			outfile1.write(">"+seqid2_short+"\n"+pepDICT[seqid2]+"\n\n")
			outfile2.write(">"+seqid1_short+"\n"+cdsDICT[seqid1]+"\n")
			outfile2.write(">"+seqid2_short+"\n"+cdsDICT[seqid2]+"\n\n")
		infile.close()
		outfile1.close()
		outfile2.close()

	if not os.path.exists(out+".ks"):
		cmd = "python synonymous_calc.py "+\
			  out+".pep.pairs.fa "+\
			  out+".cds.pairs.fa >"+\
			  out+".ks"
		print cmd
		os.system(cmd)
		#os.system("python report_ks.py "+taxon+".ks")

	#parse the ks output
	#0-name,1-dS-yn,2-dN-yn,3-dS-ng,4-dN-ng
	if not os.path.exists(out+".ks.yn") or not os.path.exists(out+".ks.ng"):
		first = True # marker to skip header
		infile = open(out+".ks","r")
		outfile1 = open(out+".ks.yn","w")
		outfile2 = open(out+".ks.ng","w")
		for line in infile:
			if first:
				first = False
				continue
			spls = line.strip().split(",")
			yn,ng = float(spls[1]),float(spls[3])
			if yn > 0.0 and yn < 0.5:
				outfile1.write(spls[1]+"\n")
			if ng > 0.0 and ng < 0.5:
				outfile2.write(spls[3]+"\n") 
		infile.close()
		outfile1.close()
		outfile2.close()
		
	with open(out+".R","w") as f: 
		f.write("pdf=pdf(file='"+out+".ks.ng.pdf',width=7,height=7)\n")
		f.write("a<-read.table('"+out+".ks.ng')\n")
		f.write("hist(a[,1],breaks=50,col='grey',xlab='',ylab='',main='")
		f.write(taxon1+"_"+taxon2+"',axes=FALSE)\n")
		f.write("axis(1,pos=0)\naxis(2,pos=0)\ndev.off()\n\n")
