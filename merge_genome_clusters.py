"""
- Given a dir of _1.subtrees, each is from a beet bait
- extract the Caryophyllales clade containing the bait beet gene
- Output a summary file. Each line has all beet genes in a Caryophyllales clade
- Read in the summary file and make single linkage clusters
"""

CARY = ["Beta","Dica","Spol"]

EUDICOT = ["Achn","Cila","Coca","Nenu","Rara",\
		   "Utri","Caan","Acoerulea","Athaliana","Brapa",\
		   "Bstricta","Cclementina","Cpapaya","Crubella","Csativus",\
		   "Csinensis","Egrandis","Esalsugineum","Fvesca","Gmax",\
		   "Graimondii","Lusitatissimum","Mesculenta","Mguttatus","Mtruncatula",\
		   "Ppersica","Ptrichocarpa","Rcommunis","Slycopersicum","Spurpurea",\
		   "Tcacao","Vvinifera"]

MONOCOT = ["Musa","Pheq","Phoe","Elgu","Osativa",\
		   "Spolyrhiza","Zmays"]

BASAL_ANG = ["Atrichopoda"] # basal angiosperm

import phylo3,newick3,os,sys
import tree_utils
import networkx as nx

def get_beta_geneid(longid):
	"""input longid looks like Beta@Bv3_054900_nows_t1
	or Bv3_054900_nows.raxml.tre.tt.mm
	or Bv1_000120_fdjg_1.subtree
	extract the unique beta gene id Bv3_054900_nows"""
	if "@" in longid: # for seqid
		longid = longid.split("@")[1]
	longid = longid.split(".")[0] # for file names
	# _t1, _t2... all are the same gene
	spls = (longid.split(".")[0]).split("_")
	if len(spls) >= 3:
		return spls[0]+"_"+spls[1]+"_"+spls[2]
	else: return None
	
def extract_target_clade(root,ingroups,outgroups,target_geneid):
	"""
	doesn't have to be rooted and doesn't have to have any outgroup
	get the most inclusive ingroup clade that contains the target
	score is number of tips, regardless of number of taxa
	"""
	# Check to make sure that target_geneid in tree
	all_labels = tree_utils.get_front_labels(root)
	target_label = ""
	for label in all_labels:
		if get_beta_geneid(label) == target_geneid:
			target_label = label
			break
	assert target_label != "", "cannot find target gene id in tree"
	
	# Have any outgroup?
	found_outgroup = False
	for label in all_labels:
		if tree_utils.get_name(label) in outgroups:
			found_outgroup = True
			break
	if not found_outgroup:
		return root # no outgroup, return tree unchanged
	else:
		if root.nchildren == 2:
			node,root = tree_utils.remove_kink(root,root)
		inclades = []
		while True:
			max_score,direction,max_node = 0,"",None
			# score is number of tips
			# not number of taxa
			# different from the standard function in tree_utils
			for node in root.iternodes():
				front,back = 0,0
				for name in tree_utils.get_front_names(node):
					if name in outgroups:
						front = -1
						break
					elif name in ingroups: front += 1
					else: sys.exit("Check taxonID "+name)
				for name in tree_utils.get_back_names(node,root):
					if name in outgroups:
						back = -1
						break
					elif name in ingroups: back += 1
					else: sys.exit("Check taxonID "+name)
				if front > max_score:
					max_score,direction,max_node = front,"front",node
				if back > max_score:
					max_score,direction,max_node = back,"back",node
			#print max_score,direction
			if max_score > 0:
				if direction == "front":
					inclades.append(max_node)
					kink = max_node.prune()
					if len(root.leaves()) > 3:
						newnode,root = tree_utils.remove_kink(kink,root)
					else: break
				elif direction == "back":
					par = max_node.parent
					par.remove_child(max_node)
					max_node.prune()
					inclades.append(phylo3.reroot(root,par))#flip dirction
					if len(max_node.leaves()) > 3:
						max_node,root = tree_utils.remove_kink(max_node,max_node)
					else: break
			else: break

		for inclade in inclades:
			print tree_utils.get_front_labels(inclade)
			for label in tree_utils.get_front_labels(inclade):
				if label == target_label:
					return inclade
	
def to_edges(l):
	"""
	convert a list ['a','b','c','d'] to [(a,b),(b,c),(c,d)]
'	"""
	it = iter(l)
	last = next(it)
	for current in it:
		yield last, current
		last = current

if __name__ == "__main__":
	if len(sys.argv) != 2:
		print "python merge_genome_clusters.py subtreeDIR"
		sys.exit(0)
	
	indir = sys.argv[1]
	
	# extract Caryophyllales lineages that containing the bait from phylomes
	outfile = open("beet_clusters","w")
	for i in os.listdir(indir): # loop through the trees
		target_geneid = get_beta_geneid(i)
		print target_geneid
		with open(indir+"/"+i,"r") as infile:
			tree = newick3.parse(infile.readline())
		target_clade = extract_target_clade(root=tree,\
				ingroups=CARY,\
				outgroups=EUDICOT+MONOCOT+BASAL_ANG,\
				target_geneid=target_geneid)
		for label in tree_utils.get_front_labels(target_clade):
			if tree_utils.get_name(label) == "Beta":
				print label
				outfile.write(label+"\t")
		outfile.write("\n")
	outfile.close()
	
	# find beet locus ids that belong to the same lineage
	# according to phylomes
	G = nx.Graph() # initiate the graph
	infile = open("beet_clusters","r")
	for line in infile:
		if len(line) < 3: continue
		spls = line.strip().split("\t")
		G.add_nodes_from(spls)
		G.add_edges_from(to_edges(spls))
	infile.close()
	
	# group beet locus ids that belong to the same Caryophyllales lineage
	with open("beet_clusters_merged","w") as outfile:
		for cc in nx.connected_components(G):
			for name in cc:
				outfile.write(name+"\t")
			outfile.write("\n")
	
	"""
	single linkage clustering produces a few really large clusters
	of peroxidase. Pulled out the clusters that doesn't completely overlap
	and check them manually. Chimeric sequences may be a problem in
	beet genome annotation (or they are real fusion genes)
	
	All the single linkage clusters turn out to be quite reasonable. Some
	are form short vs. long sequences and some are likely alignment error
	"""

			
	


		
			
