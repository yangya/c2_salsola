import phylo3,newick3,os,sys

if __name__ == "__main__":
	if len(sys.argv) != 2:
		print "python remove_output_tree_labels intre"
		sys.exit(0)
	

	with open(sys.argv[1],"r") as infile:
		 intree = newick3.parse(infile.readline())
	
	for node in intree.iternodes():
		if not node.istip:
			node.label = None
		node.length = 1
		
	with open(sys.argv[1]+".out","w") as outfile:
		outfile.write(newick3.tostring(intree)+";\n")
