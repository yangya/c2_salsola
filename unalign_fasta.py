import sys,os
from Bio import SeqIO

if __name__ =="__main__":
	if len(sys.argv) != 2:
		print "usage: unalign_fasta.py dir"
		sys.exit()
	
	DIR = sys.argv[1]
	if DIR[-1] != "/": DIR = DIR+"/"
	for i in os.listdir(DIR):
		print i
		handle = open(DIR+i,"r")
		outfile = open(DIR+i+".fa","w")
		for s in SeqIO.parse(handle,"fasta"):
			seqid = str(s.id).split("/")[0]
			seq = str(s.seq).replace("-","")
			print seqid
			print seq
			outfile.write(">"+seqid+"\n"+seq+"\n")
		outfile.close()
		handle.close()
