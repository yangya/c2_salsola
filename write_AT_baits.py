import sys,os
from Bio import SeqIO

if __name__ =="__main__":
	if len(sys.argv) != 3:
		print "usage: write_AT_baits.py infasta outdir"
		sys.exit()
	
	infasta,outdir = sys.argv[1:]
	handle = open(infasta)
	for s in SeqIO.parse(handle,"fasta"):
		locusid = s.id.replace(">","").split(".")[0]
		print locusid
		with open(outdir+"/"+locusid+".pep.fa","a") as outfile:
			outfile.write(">"+str(s.id)+"\n"+str(s.seq)+"\n")
	handle.close()

